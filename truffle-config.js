require('dotenv').config();
const { PASS_PHRASE, ALCHEMY_MUMBAI_API_KEY } = process.env;

const HDWalletProvider = require('@truffle/hdwallet-provider');

module.exports = {
	networks: {
		mumbai: {
			provider: () => new HDWalletProvider(PASS_PHRASE, `https://polygon-mumbai.g.alchemy.com/v2/${ALCHEMY_MUMBAI_API_KEY}`),
			network_id: 80001,
			confirmations: 2,
			timeoutBlocks: 200,
			skipDryRun: true
		},
	},
	mocha: {
		// timeout: 100000
	},
	compilers: {
		solc: {
			version: "0.8.19",
		}
	},
};
