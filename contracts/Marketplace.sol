// SPDX-License-Identifier: MIT

pragma solidity ^0.8.19;

import "@openzeppelin/contracts/access/Ownable.sol";

contract Marketplace is Ownable {
    enum ShippingStatus {
        Pending,
        Shipped,
        Delivered
    }

    event StatusChanged(
        ShippingStatus previousStatus,
        ShippingStatus newStatus
    );
    event MissionCompleted();

    ShippingStatus private _status;

    mapping(address => bool) private _customers;

    constructor() {
        _status = ShippingStatus.Pending;
    }

    modifier onlyCustomer() {
        require(
            _customers[msg.sender] == true,
            "Seul le client peut voir le statut de la commande."
        );
        _;
    }

    function shipped() public onlyOwner {
        ShippingStatus previousStatus = _status;
        _status = ShippingStatus.Shipped;
        emit StatusChanged(previousStatus, _status);
    }

    function delivered() public onlyOwner {
        ShippingStatus previousStatus = _status;
        _status = ShippingStatus.Delivered;
        emit StatusChanged(previousStatus, _status);
        emit MissionCompleted();
    }

    function addCustomer(address customer) public onlyOwner {
        _customers[customer] = true;
    }

    function removeCustomer(address customer) public onlyOwner {
        _customers[customer] = false;
    }

    function getStatus() public view onlyOwner returns (ShippingStatus) {
        return _status;
    }

    function getStatusForCustomer()
        public
        payable
        onlyCustomer
        returns (ShippingStatus)
    {
        require(
            msg.value == 1 ether,
            "Veuillez payer 1 ether pour voir le status de cette commande."
        );
        return _status;
    }
}
